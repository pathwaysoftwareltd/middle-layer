import gulp from 'gulp';

import del from 'del';
import babel from 'gulp-babel';
import eslint from 'gulp-eslint';
import plumber from 'gulp-plumber';
import watch from 'gulp-watch';
import runSequence from 'run-sequence';

const SRC_PATH = './src',
	BUILD_PATH = './lib';

gulp.task('build:clear', function() {
	return del([ BUILD_PATH + '/*' ]);
});

gulp.task('build:scripts', function() {
	return gulp.src(SRC_PATH + '/**/*.js')
		.pipe(eslint())
		.pipe(babel())
		.pipe(plumber())
		.pipe(gulp.dest(BUILD_PATH));
});

gulp.task('build', function(cb) {
	runSequence(
		[ 'build:clear' ],
		[ 'build:scripts' ],
		cb);
});

gulp.task('watch', function(cb) {
	runSequence(
		[ 'build' ],
		function() {
			watch([ SRC_PATH + '/**/*.js' ], function() { gulp.start('build'); });

			cb();
		}
	);
});

gulp.task('default', function() {
	gulp.start('build');
});
