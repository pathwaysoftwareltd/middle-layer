export function route(route) {
	return function(target, key, descriptor) {
		const fn = descriptor.value || descriptor.get;

		delete descriptor.value;
		delete descriptor.writable;

		if (!route) {
			route = key;
		}

		descriptor.get = function() {
			const bound = fn.bind(this, route);

			Object.defineProperty(this, key, {
				configurable: true,
				writable: true,
				value: bound
			});

			return bound;
		};

		if (!target.routes) {
			target.routes = [];
		}

		target.routes.push({ match: route, action: key });

	};
}

export class MlController {

	actions() {
		return [];
	}

	constructor(app = {}) {
		this.app = app;
	}

	render(template, data = {}) {
		return this.app.renderer.render(template, data);
	}

}
