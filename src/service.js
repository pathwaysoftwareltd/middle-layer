import { MlConfig } from './config';

export class MlService extends MlConfig {

	config() {
		return {
			model: null,
			proxy: null
		};
	}

	convertor(val) {
		return this.model ? new this.model().convert(val) : val;
	}

	convertedPromise(promise) {
		return promise.then((r) => { return this.convertor(r); });
	}

	async proxyInstance() {
		if (this._proxy === undefined) {
			this._proxy = new this.proxy();
		}

		return this._proxy;
	}

	async proxyCall(func) {
		const proxyInstance = await this.proxyInstance(),
			val = await func(proxyInstance);

		return this.convertor(val);
	}

}
