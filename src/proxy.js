// @flow
let hostIndex: number = 0;

export class MlProxy {

	_host: string|[string]|null;

	constructor() {
		this._host = null;
	}

	headers() {
		return {};
	}

	authHeaders() {
		return {};
	}

	allHeaders(includeAuth: boolean) {
		return Object.assign({}, this.headers(), ( includeAuth ? this.authHeaders(): {} ));
	}

	singleRequest(host: string, endPoint: string, data: ?Object = null, opts: Object = {}) {
		opts = Object.assign({
			url: `${host}${endPoint}`,
			data: data,
			method: 'GET',
			auth: true,
			dataType: 'json',
			timeout: 10000
		}, opts);

		const headers = this.allHeaders(opts.auth);
		if (headers) {
			if (opts.headers) {
				Object.assign(opts.headers, headers);
			}
			else {
				opts.headers = headers;
			}
		}

		// $FlowFixMe
		return $.ajax(opts).promise();
	}

	async request(endPoint: string, data: ?Object = null, opts: Object = {}) {
		if (this._host === null) {
			throw new Error('No host defined');
		}

		if (typeof this._host === 'string') {
			return await this.singleRequest(this._host, endPoint, data, opts);
		}
		else {
			const startingHostIndex = hostIndex;
			let host = this._host[startingHostIndex];

			let firstError;

			do {
				let response;
				try {
					return await this.singleRequest(host, endPoint, data, opts);
				}
				catch (err) {
					if (!firstError) {
						firstError = err;
					}
				}

				host = this.rotateHost();
			} while (hostIndex !== startingHostIndex);

			throw firstError;
		}

	}

	rotateHost() {
		if (this._host === null) {
			throw new Error('No host defined');
		}
		
		if (typeof this._host === 'string') {
			return this._host;
		}

		if (this._host.length === 1) {
			return this._host[0];
		}

		if (hostIndex === this._host.length - 1) {
			return this._host[hostIndex = 0];
		}

		return this._host[++hostIndex];
	}

	getCurrentHost() {
		return Array.isArray(this._host) ? this._host[hostIndex] : this._host;
	}

	jsonFail(resolve: (Object) => {}) {
		return function(xhr: JQueryXHR) {
			resolve(xhr.responseJSON || {});
		};
	}

}
