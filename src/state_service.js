import { MlService } from './service';

export class MlStateService extends MlService {

	config() {
		const c = super.config();

		c.state = null;
		c.idField = null;

		return c;
	}

	addToState(resource) {
		this.state[resource[this.idField]] = resource;
	}

	addAllToState(collection) {
		let self = this;

		collection.forEach(function(resource) {
			self.addToState(resource);
		});

		return Promise.resolve(collection);
	}

	getFromState(id) {
		let self = this;
		return Promise.resolve(self.state[id]);
	}

	removeFromState(id) {
		if (this.state[id]) {
			delete this.state[id];
		}
	}

}
