// @flow
import { MlConfig } from './config';
import { MlProgress } from './progress';
import { MlPubSub } from './pub_sub';

export class MlApplication extends MlConfig {

	// Config

	config() {
		return {
			controllers: [],
			classes: {
				router: require('./router').MlRouter,
				renderer: require('./renderer').MlRenderer
			}
		};
	}


	constructor() {
		super();

		this.progress = new MlProgress();
		this.event = new MlPubSub();

		this.router = new this.classes.router({ app: this, controllers: this.controllers });

		if (this.classes.renderer) {
			this.renderer = new this.classes.renderer();
		}

		this.state = {};
	}


	// Overrides

	async ready(onDevice: boolean) {
		this.onDevice = onDevice;
	}

	afterReady() {
		// Disable splash screen
		// $FlowFixMe
		if (typeof navigator !== 'undefined' && navigator.splashscreen && typeof navigator.splashscreen.hide === 'function') {
			// $FlowFixMe
			navigator.splashscreen.hide();
		}
	}


	// Helpers

	static doAppReady() {
		global.app = new this();

		function appReady(onDevice) {
			return function() {
				global.app.ready(onDevice)
					.then(() => global.app.afterReady());
			};
		}

		if (typeof global.cordova !== 'undefined') {
			document.addEventListener('deviceready', appReady(true), false);
		}
		else {
			$(appReady(false));
		}
	}

}
