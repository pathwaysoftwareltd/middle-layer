import { MlConfig } from './config';
import { MlFileSystem } from './file_system';
import { MlProxy } from './proxy';

export class MlDownloader extends MlConfig {

	static canDownload() {
		return typeof FileTransfer !== 'undefined' ||
			typeof requestFileSystem !== 'undefined' ||
			typeof webkitRequestFileSystem !== 'undefined';
	}

	config() {
		return {
			namespace: 'namespace',
			rootPath: 'cdvfile://localhost/persistent',
			fileSystem: {
				class: MlFileSystem,
				local: window.PERSISTENT
			},
			proxy: {
				class: MlProxy
			}
		};
	}

	getPath(remoteUrl) {
		return this.stripHost(remoteUrl);
	}

	stripHost(path) {
		for (let host of this.hosts) {
			path = path.replace(host, '');
		}

		return path;
	}

	download(remoteUrl, path, includeAuth) {
		if (typeof FileTransfer !== 'undefined') {
			return this.downloadFT(remoteUrl, path, includeAuth);
		}
		else {
			return this.downloadAjax(remoteUrl, path, includeAuth);
		}
	}

	downloadOnly(remoteUrl, includeAuth) {
		return new Promise((resolve, reject) => {
			const request = new XMLHttpRequest();

			request.open('GET', remoteUrl, true);
			request.responseType = 'blob';

			request.onerror = reject;
			request.onload = () => {
				if (request.status >= 400) {
					reject(request.response);
				}
				else {
					resolve(request.response);
				}
			};

			let headers = new this.proxy.class().allHeaders(includeAuth);
			for (let header in headers) {
				request.setRequestHeader(header, headers[header]);
			}

			request.send();
		});
	}

	async downloadAjax(remoteUrl, path, includeAuth) {
		const fs = new this.fileSystem.class();

		path = path || this.getPath(remoteUrl);

		const blob = await this.downloadOnly(remoteUrl, path, includeAuth);

		let dir = await fs.request(this.fileSystem.local);
		dir = await fs.mkdirp(dir, path);
		return await fs.saveBlob(dir, blob, this.fileSystem.class.info(path).fileName);
	}

	downloadFT(remoteUrl, path, includeAuth) {
		const me = this;

		let fs = new me.fileSystem.class();

		path = path || me.getPath(remoteUrl);

		return fs.request(me.fileSystem.local)
			.then(function (dir) {
				return fs.mkdirp(dir, path);
			})
			.then(function (dir) {
				let fileTransfer = new FileTransfer(),
					opts = { headers: new me.proxy.class().allHeaders(includeAuth) },
					newPath = `${me.rootPath}${dir.fullPath}${me.fileSystem.class.info(path).fileName}`;

				return new Promise(function (resolve, reject) {
					new FileTransfer().download(
						remoteUrl,
						newPath,
						resolve,
						reject,
						true,
						opts
					);
				});
			});
	}

}
