// @flow
import async from 'async';
import { MlConfig } from './config';

export class MlFileSystem extends MlConfig {

	config() {
		return {
			root: 'NoCloud/mlapp'
		};
	}

	request(localFileSystem, size = 0, quotaSize = null) {
		const me = this;

		if (quotaSize === null) {
			if (size > 0) {
				quotaSize = size;
			}
			else {
				quotaSize = 61644800;
			}
		}

		const doRequest = async function () {
			const fs = await me.requestFileSystem(localFileSystem, size),
				dir = await me.mkdirp(fs.root, me.root);

			await me.getFile(dir, '.nomedia', { create: true });

			return dir;
		};

		if (typeof navigator !== 'undefined' && navigator.webkitPersistentStorage) {
			return new Promise(function (resolve, reject) {
				let storage = localFileSystem === global.PERSISTENT
					? navigator.webkitPersistentStorage
					: navigator.webkitTemporaryStorage;

				storage.requestQuota(
					quotaSize,
					function (grantedBytes) {
						me.grantedBytes = grantedBytes;
						resolve();
					},
					reject
				);
			})
				.then(() => { return doRequest(); });
		}
		else {
			return doRequest();
		}
	}

	ls(dir) {
		return new Promise(function (resolve, reject) {
			dir.createReader().readEntries(resolve, reject);
		});
	}

	async mkdirp(dir, path) {
		const info = MlFileSystem.info(path);

		let currentDir = dir;
		for (const subDir of info.dirs) {
			currentDir = await this.getDirectory(currentDir, subDir, { create: true });
		}

		return currentDir;
	}

	saveBlob(dir, blob, path) {
		return new Promise(function (resolve, reject) {
			async.waterfall([
				function (callback) {
					dir.getFile(path, { create: true },
						(entry) => { callback(null, entry); },
						callback
					);
				},
				function (entry, callback) {
					entry.createWriter(
						function (writer) {
							writer.onwriteend = () => { callback(null, entry); };
							writer.onerror = callback;

							writer.write(blob);
						},
						callback
					);
				}
			], function (err, entry) {
				if (err) {
					reject(err);
				}
				else {
					resolve(entry);
				}
			});

		});
	}

	async findOrCreate(dir, filePath) {
		try {
			return await this.getFile(dir, filePath, {});
		}
		catch (ex) {
			if (ex.code === 1) {
				return await this.getFile(dir, filePath, { create: true });
			}
			else {
				throw ex;
			}
		}
	}

	static info(path) {
		let dirs = path.split('/'),
			lastIdx = dirs.length - 1,
			fileName = dirs[lastIdx];

		let dotIdx = fileName.indexOf('.'),
			name = fileName.substring(0, dotIdx),
			ext = fileName.substring(dotIdx);

		return {
			dirs: dotIdx === -1 ? dirs : dirs.slice(0, lastIdx),
			name: name,
			ext: ext,
			fileName: fileName
		};
	}

	// FS Promise Wrappers
	requestFileSystem(type, size) {
		return new Promise(function (resolve, reject) {
			(global.requestFileSystem || global.webkitRequestFileSystem)
				(type, size, resolve, reject);
		});
	}

	getDirectory(currentDir, subDir, options) {
		return new Promise(function (resolve, reject) {
			currentDir.getDirectory(
				subDir,
				options,
				function (newDir) {
					resolve(newDir);
				},
				function (err) {
					reject(err);
				}
			);
		});
	}

	getFile(currentDir, filePath, options) {
		return new Promise(function (resolve, reject) {
			currentDir.getFile(
				filePath,
				options,
				function (fileEntry) {
					resolve(fileEntry);
				},
				function (err) {
					reject(err);
				}
			);
		});
	}

	static test() {
		new MlFileSystem().request(global.PERSISTENT)
			.then(function (dir) { new MlFileSystem().supportsCdv(dir).then(function () { console.log(arguments); }); return dir; })
			.then(function (dir) { return new MlFileSystem().mkdirp(dir, 'assets'); })
			.then(function (dir) { console.log(dir.fullPath); return new MlFileSystem().ls(dir); })
			.then(function (entries) { console.log(entries); return entries; })
			.catch(function () { console.log(arguments); });
	}

}
