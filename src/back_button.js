import { MlPubSub } from './pub_sub';

export class MlBackButton {

	constructor() {
		this.attached = false;
		this.event = new MlPubSub();
		this.boundHandler = this.handler.bind(this);
	}

	attach() {
		if (this.attached) {
			return false;
		}

		document.addEventListener('backbutton', this.boundHandler, false);
		this.attached = true;
	}

	detach() {
		if (!this.attached) {
			return false;
		}

		document.removeEventListener('backbutton', this.boundHandler, false);
		this.attached = false;
	}

	subscribe(func) {
		return this.event.subscribe(0, func);
	}

	handler(evt) {
		evt.preventDefault();
		evt.stopPropagation();

		this.event.publish(0, evt);
	}

}
