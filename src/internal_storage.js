// @flow
export class MlInternalStorage {
	_key: string
	_storage: Object
	_sortField: ?string

	// Config
	config(key: string) {
		this._key = key || 'store';

		this._storage = localforage;
	}

	setSortField(field: string) {
		this._sortField = field;
	}

	// Contstructor
	constructor(key: string) {
		this.config(key);

		this._sortField = null;
	}

	// Private
	_serialize(data: any) {
		return data;
	}

	_deserialize(value: any) {
		return value;
	}

	_getRecordKey(id: string) {
		return this._key + '-' + id;
	}

	_getIndexKey() {
		return this._key + '_index';
	}

	async _getIndex(): Promise<Array<string>> {
		const item = await this._storage.getItem(this._getIndexKey());
		return item ? this._deserialize(item) : [];
	}

	async _setIndex(array: Array<string>) {
		if (!array) {
			return;
		}

		const obj = this._serialize(array);
		return await this._storage.setItem(this._getIndexKey(), obj);
	}

	async _addToIndex(recordKey: string) {
		const index = await this._getIndex();
		if (!index || index.includes(recordKey)) {
			return;
		}

		index.push(recordKey);
		await this._setIndex(index);
	}

	async _removeFromIndex(recordKey: string) {
		const index = await this._getIndex(),
			idx = index ? index.indexOf(recordKey) : -1;

		if (idx !== -1) {
			index.splice(idx, 1);
		}

		await this._setIndex(index);
	}

	async _hasKey(id: string) {
		const index = await this._getIndex();
		return index.includes(this._getRecordKey(id));
	}

	async _getRecord(recordKey: string) {
		const record = await this._storage.getItem(recordKey);
		return this._deserialize(record);
	}

	async _setRecord(recordKey: string, data: Object) {
		await this._storage.setItem(recordKey, this._serialize(data));
		await this._addToIndex(recordKey);
	}

	async _removeRecord(recordKey: string) {
		await this._storage.removeItem(recordKey);
		await this._removeFromIndex(recordKey);
	}

	async _sort() {
		if (!this._sortField) {
			return;
		}

		const array = await this._getAll();
		array.sort((a, b) => +(a[this._sortField] > b[this._sortField]));

		const newIndex = array.map((x) => this._getRecordKey(x.id));

		await this._setIndex(newIndex);
	}

	async _getAll(): Promise<Array<Object>> {
		const index = await this._getIndex();
		let rv: Array<Object> = [];

		for (let recordKey of index) {
			rv.push(await this._getRecord(recordKey));
		}

		return rv;
	}

	async _setAll(array: Array<Object>, clearFirst?: boolean = false) {
		if (clearFirst) {
			await this._clear();
		}

		for (let obj of array) {
			const recordKey = this._getRecordKey(obj.id);

			await this._setRecord(recordKey, obj);
			await this._addToIndex(recordKey);
		}
	}

	async _clear() {
		const index = await this._getIndex();

		for (let recordKey of index) {
			await this._storage.removeItem(recordKey);
		}

		await this._storage.removeItem(this._getIndexKey());
	}

	// Public
	has(id: string) {
		return this._hasKey(id);
	}

	get(id?: ?string) {
		if (id === undefined || id === null) {
			return this._getAll();
		}
		else {
			return this._getRecord(this._getRecordKey(id));
		}
	}

	async first() {
		const collection = await this._getAll();
		return collection && collection.length > 0 ? collection[0] : null;
	}

	async set(id: string | Object, data: Object | boolean = false) {
		if (typeof id === 'object') {
			if (Array.isArray(id) && typeof data === 'boolean') {
				return await this._setAll(id, data);
			}
			else if (id.id !== undefined) {
				return await this._setRecord(this._getRecordKey(id.id), id);
			}
			else {
				for (let key of Object.keys(id)) {
					await this._setRecord(this._getRecordKey(key), id[key]);
				}
			}
		}
		else {
			return await this._setRecord(this._getRecordKey(id), data);
		}
	}

	async update(id: string, data: Object) {
		const recordKey = this._getRecordKey(id),
			record = await this._getRecord(recordKey);

		if (record) {
			Object.assign(record, data);
			await this._setRecord(recordKey, record);;
		}
	}

	remove(id: string) {
		return this._removeRecord(this._getRecordKey(id));
	}

	clear() {
		return this._clear();
	}

	async count() {
		const index = await this._getIndex();
		return index ? index.length : 0;
	}

	sort() {
		return this._sort();
	}

}
