export class MlConfig {

	config() {
		return {};
	}

	constructor() {
		let config = this.config();

		if (super.config) {
			config = Object.assign({}, super.config(), config);
		}

		for (let attr in config) {
			this[attr] = config[attr];
		}
	}

}
