export class MlFlash {

	static async message(message, type) {
		if (typeof navigator !== 'undefined' && navigator.notification) {
			return new Promise(function(resolve) {
				navigator.notification.alert(
					message,					// message
					function() { resolve(); },	// callback
					'Forget Me Not',			// title
					'Okay'						// buttonName
				);
			});
		}
		else {
			alert(message);
		}
	}

	static alert(message) {
		return this.message(message, 'alert');
	}

	static info(message) {
		return this.message(message, 'info');
	}

}
