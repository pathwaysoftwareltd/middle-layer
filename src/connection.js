import { MlPubSub } from './pub_sub';

export class MlConnection {

	constructor() {
		this.attached = false;
		this.pluginDetected = typeof navigator !== 'undefined' && navigator.connection && typeof Connection !== 'undefined';

		this.event = new MlPubSub();

		this.boundHandler = this.handler.bind(this);
	}

	isOnline() {
		return !this.pluginDetected || navigator.connection.type !== Connection.NONE;
	}

	attach() {
		if (this.attached) {
			return false;
		}

		document.addEventListener('online', this.boundHandler, false);
		document.addEventListener('offline', this.boundHandler, false);

		this.attached = true;
	}

	detach() {
		if (!this.attached) {
			return false;
		}

		document.removeEventListener('online', this.boundHandler, false);
		document.removeEventListener('offline', this.boundHandler, false);

		this.attached = false;
	}

	subscribe(func) {
		return this.event.subscribe(0, func);
	}

	handler(evt) {
		this.event.publish(0, evt.type === 'online');
	}

}
