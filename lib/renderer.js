"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlRenderer = undefined;

var _regenerator = require("babel-runtime/regenerator");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require("babel-runtime/helpers/asyncToGenerator");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlRenderer = exports.MlRenderer = function () {
	function MlRenderer() {
		(0, _classCallCheck3.default)(this, MlRenderer);
	}

	(0, _createClass3.default)(MlRenderer, [{
		key: "render",
		value: function () {
			var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(template) {
				var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
							case "end":
								return _context.stop();
						}
					}
				}, _callee, this);
			}));

			function render(_x) {
				return _ref.apply(this, arguments);
			}

			return render;
		}()
	}]);
	return MlRenderer;
}();