"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlController = undefined;

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

exports.route = route;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function route(route) {
	return function (target, key, descriptor) {
		var fn = descriptor.value || descriptor.get;

		delete descriptor.value;
		delete descriptor.writable;

		if (!route) {
			route = key;
		}

		descriptor.get = function () {
			var bound = fn.bind(this, route);

			Object.defineProperty(this, key, {
				configurable: true,
				writable: true,
				value: bound
			});

			return bound;
		};

		if (!target.routes) {
			target.routes = [];
		}

		target.routes.push({ match: route, action: key });
	};
}

var MlController = exports.MlController = function () {
	(0, _createClass3.default)(MlController, [{
		key: "actions",
		value: function actions() {
			return [];
		}
	}]);

	function MlController() {
		var app = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
		(0, _classCallCheck3.default)(this, MlController);

		this.app = app;
	}

	(0, _createClass3.default)(MlController, [{
		key: "render",
		value: function render(template) {
			var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

			return this.app.renderer.render(template, data);
		}
	}]);
	return MlController;
}();