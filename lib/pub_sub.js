"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlPubSub = undefined;

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlPubSub = exports.MlPubSub = function () {
	function MlPubSub() {
		(0, _classCallCheck3.default)(this, MlPubSub);

		this.topics = {};
		this.hOP = this.topics.hasOwnProperty;
	}

	(0, _createClass3.default)(MlPubSub, [{
		key: "subscribe",
		value: function subscribe(topic, listener) {
			// Create the topic's object if not yet created
			if (!this.hOP.call(this.topics, topic)) {
				this.topics[topic] = [];
			}

			// Add the listener to queue
			var index = this.topics[topic].push(listener) - 1;

			// Provide handle back for removal of topic
			return {
				remove: function remove() {
					delete this.topics[topic][index];
				}
			};
		}
	}, {
		key: "publish",
		value: function publish(topic, info) {
			// If the topic doesn't exist, or there's no listeners in queue, just leave
			if (!this.hOP.call(this.topics, topic)) {
				return;
			}

			// Cycle through topics queue, fire!
			this.topics[topic].forEach(function (item) {
				item(info != undefined ? info : null);
			});
		}
	}]);
	return MlPubSub;
}();