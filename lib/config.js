"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlConfig = undefined;

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

var _get2 = require("babel-runtime/helpers/get");

var _get3 = _interopRequireDefault(_get2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlConfig = exports.MlConfig = function () {
	(0, _createClass3.default)(MlConfig, [{
		key: "config",
		value: function config() {
			return {};
		}
	}]);

	function MlConfig() {
		(0, _classCallCheck3.default)(this, MlConfig);

		var config = this.config();

		if ((0, _get3.default)(MlConfig.prototype.__proto__ || Object.getPrototypeOf(MlConfig.prototype), "config", this)) {
			config = Object.assign({}, (0, _get3.default)(MlConfig.prototype.__proto__ || Object.getPrototypeOf(MlConfig.prototype), "config", this).call(this), config);
		}

		for (var attr in config) {
			this[attr] = config[attr];
		}
	}

	return MlConfig;
}();