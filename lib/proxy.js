'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlProxy = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var hostIndex = 0;

var MlProxy = exports.MlProxy = function () {
	function MlProxy() {
		(0, _classCallCheck3.default)(this, MlProxy);

		this._host = null;
	}

	(0, _createClass3.default)(MlProxy, [{
		key: 'headers',
		value: function headers() {
			return {};
		}
	}, {
		key: 'authHeaders',
		value: function authHeaders() {
			return {};
		}
	}, {
		key: 'allHeaders',
		value: function allHeaders(includeAuth) {
			return Object.assign({}, this.headers(), includeAuth ? this.authHeaders() : {});
		}
	}, {
		key: 'singleRequest',
		value: function singleRequest(host, endPoint) {
			var data = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
			var opts = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

			opts = Object.assign({
				url: '' + host + endPoint,
				data: data,
				method: 'GET',
				auth: true,
				dataType: 'json',
				timeout: 10000
			}, opts);

			var headers = this.allHeaders(opts.auth);
			if (headers) {
				if (opts.headers) {
					Object.assign(opts.headers, headers);
				} else {
					opts.headers = headers;
				}
			}

			// $FlowFixMe
			return $.ajax(opts).promise();
		}
	}, {
		key: 'request',
		value: function () {
			var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(endPoint) {
				var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
				var opts = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
				var startingHostIndex, host, firstError, response;
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								if (!(this._host === null)) {
									_context.next = 2;
									break;
								}

								throw new Error('No host defined');

							case 2:
								if (!(typeof this._host === 'string')) {
									_context.next = 8;
									break;
								}

								_context.next = 5;
								return this.singleRequest(this._host, endPoint, data, opts);

							case 5:
								return _context.abrupt('return', _context.sent);

							case 8:
								startingHostIndex = hostIndex;
								host = this._host[startingHostIndex];
								firstError = void 0;

							case 11:
								response = void 0;
								_context.prev = 12;
								_context.next = 15;
								return this.singleRequest(host, endPoint, data, opts);

							case 15:
								return _context.abrupt('return', _context.sent);

							case 18:
								_context.prev = 18;
								_context.t0 = _context['catch'](12);

								if (!firstError) {
									firstError = _context.t0;
								}

							case 21:

								host = this.rotateHost();

							case 22:
								if (hostIndex !== startingHostIndex) {
									_context.next = 11;
									break;
								}

							case 23:
								throw firstError;

							case 24:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, this, [[12, 18]]);
			}));

			function request(_x3) {
				return _ref.apply(this, arguments);
			}

			return request;
		}()
	}, {
		key: 'rotateHost',
		value: function rotateHost() {
			if (this._host === null) {
				throw new Error('No host defined');
			}

			if (typeof this._host === 'string') {
				return this._host;
			}

			if (this._host.length === 1) {
				return this._host[0];
			}

			if (hostIndex === this._host.length - 1) {
				return this._host[hostIndex = 0];
			}

			return this._host[++hostIndex];
		}
	}, {
		key: 'getCurrentHost',
		value: function getCurrentHost() {
			return Array.isArray(this._host) ? this._host[hostIndex] : this._host;
		}
	}, {
		key: 'jsonFail',
		value: function jsonFail(resolve) {
			return function (xhr) {
				resolve(xhr.responseJSON || {});
			};
		}
	}]);
	return MlProxy;
}();