'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlImportStore = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _get2 = require('babel-runtime/helpers/get');

var _get3 = _interopRequireDefault(_get2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _async = require('async');

var _async2 = _interopRequireDefault(_async);

var _internal_storage = require('./internal_storage');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlImportStore = exports.MlImportStore = function (_MlInternalStorage) {
	(0, _inherits3.default)(MlImportStore, _MlInternalStorage);

	function MlImportStore() {
		(0, _classCallCheck3.default)(this, MlImportStore);
		return (0, _possibleConstructorReturn3.default)(this, (MlImportStore.__proto__ || Object.getPrototypeOf(MlImportStore)).apply(this, arguments));
	}

	(0, _createClass3.default)(MlImportStore, [{
		key: 'config',


		// Config
		value: function config(key) {
			var fileRefs = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

			(0, _get3.default)(MlImportStore.prototype.__proto__ || Object.getPrototypeOf(MlImportStore.prototype), 'config', this).call(this, key);

			this._fileRefs = fileRefs;
		}
	}, {
		key: 'fileRefs',
		value: function fileRefs() {
			return this._fileRefs;
		}
	}, {
		key: 'destroy',
		value: function destroy(deletions) {
			var self = this;

			return new Promise(function (resolve, reject) {
				_async2.default.eachSeries(deletions, function (id, cb) {
					self.remove(id).then(cb);
				}, function (err) {
					resolve();
				});
			});
		}
	}, {
		key: 'import',
		value: function _import(data) {
			var self = this;

			var changeFunc = function changeFunc(changeCb) {
				_async2.default.eachSeries(Object.keys(data.changes), function (id, cb) {
					self.set(id, data.changes[id]).then(cb);
				}, function (err) {
					changeCb();
				});
			};

			return new Promise(function (resolve, reject) {
				if (data.deletions) {
					self.destroy(data.deletions).then(function () {
						changeFunc(resolve);
					});
				} else {
					changeFunc(resolve);
				}
			});
		}
	}]);
	return MlImportStore;
}(_internal_storage.MlInternalStorage);