"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlUtil = undefined;

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlUtil = exports.MlUtil = function () {
	function MlUtil() {
		(0, _classCallCheck3.default)(this, MlUtil);
	}

	(0, _createClass3.default)(MlUtil, null, [{
		key: "emptyPromise",
		value: function emptyPromise(val) {
			return new Promise(function (resolve) {
				resolve(val);
			});
		}
	}]);
	return MlUtil;
}();