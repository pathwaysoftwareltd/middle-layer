'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlProgress = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _pub_sub = require('./pub_sub');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlProgress = exports.MlProgress = function () {
	(0, _createClass3.default)(MlProgress, [{
		key: '_reset',
		value: function _reset() {
			this._total = 0;
			this._downloaded = 0;
		}
	}]);

	function MlProgress() {
		(0, _classCallCheck3.default)(this, MlProgress);

		var self = this;

		self.event = new _pub_sub.MlPubSub();

		self._total = 0;
		self._downloaded = 0;
		self._idx = Math.random().toString(36).substr(2, 5);
		self._key = self._idx + '_progress_';
	}

	(0, _createClass3.default)(MlProgress, [{
		key: 'percentage',
		value: function percentage() {
			var dec = this._downloaded / this._total;

			if (isNaN(dec)) {
				return 0;
			} else {
				return this._downloaded / this._total * 100;
			}
		}
	}, {
		key: 'add',
		value: function add(value) {
			this._total += value;

			this.trigger('change');

			return this;
		}
	}, {
		key: 'increment',
		value: function increment() {
			++this._total;

			this.trigger('change');

			return this;
		}
	}, {
		key: 'decrement',
		value: function decrement() {
			++this._downloaded;

			this.trigger('change');

			return this;
		}
	}, {
		key: 'start',
		value: function start() {
			this._reset();

			this.trigger('start');

			return this;
		}
	}, {
		key: 'complete',
		value: function complete() {
			this._total = this._downloaded;

			this.trigger('complete');

			this._reset();

			return this;
		}
	}, {
		key: 'on',
		value: function on(eventName, listener) {
			this.event.subscribe('' + this._key + eventName, listener);

			return this;
		}
	}, {
		key: 'trigger',
		value: function trigger(eventName) {
			this.event.publish('' + this._key + eventName, { total: this._total, downloaded: this._downloaded, percentage: this.percentage() });

			return this;
		}
	}]);
	return MlProgress;
}();