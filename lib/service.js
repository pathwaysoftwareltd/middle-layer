'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlService = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _config = require('./config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlService = exports.MlService = function (_MlConfig) {
	(0, _inherits3.default)(MlService, _MlConfig);

	function MlService() {
		(0, _classCallCheck3.default)(this, MlService);
		return (0, _possibleConstructorReturn3.default)(this, (MlService.__proto__ || Object.getPrototypeOf(MlService)).apply(this, arguments));
	}

	(0, _createClass3.default)(MlService, [{
		key: 'config',
		value: function config() {
			return {
				model: null,
				proxy: null
			};
		}
	}, {
		key: 'convertor',
		value: function convertor(val) {
			return this.model ? new this.model().convert(val) : val;
		}
	}, {
		key: 'convertedPromise',
		value: function convertedPromise(promise) {
			var _this2 = this;

			return promise.then(function (r) {
				return _this2.convertor(r);
			});
		}
	}, {
		key: 'proxyInstance',
		value: function () {
			var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								if (this._proxy === undefined) {
									this._proxy = new this.proxy();
								}

								return _context.abrupt('return', this._proxy);

							case 2:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, this);
			}));

			function proxyInstance() {
				return _ref.apply(this, arguments);
			}

			return proxyInstance;
		}()
	}, {
		key: 'proxyCall',
		value: function () {
			var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(func) {
				var proxyInstance, val;
				return _regenerator2.default.wrap(function _callee2$(_context2) {
					while (1) {
						switch (_context2.prev = _context2.next) {
							case 0:
								_context2.next = 2;
								return this.proxyInstance();

							case 2:
								proxyInstance = _context2.sent;
								_context2.next = 5;
								return func(proxyInstance);

							case 5:
								val = _context2.sent;
								return _context2.abrupt('return', this.convertor(val));

							case 7:
							case 'end':
								return _context2.stop();
						}
					}
				}, _callee2, this);
			}));

			function proxyCall(_x) {
				return _ref2.apply(this, arguments);
			}

			return proxyCall;
		}()
	}]);
	return MlService;
}(_config.MlConfig);