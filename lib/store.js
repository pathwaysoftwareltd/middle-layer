'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlStore = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _internal_storage = require('./internal_storage');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlStore = exports.MlStore = function () {
	function MlStore() {
		var key = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'store';
		(0, _classCallCheck3.default)(this, MlStore);

		this._storage = new _internal_storage.MlInternalStorage(key);
	}

	(0, _createClass3.default)(MlStore, [{
		key: 'clear',
		value: function clear() {
			return this._storage.clear();
		}
	}]);
	return MlStore;
}();