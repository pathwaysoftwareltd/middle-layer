'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlFileSystem = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _async = require('async');

var _async2 = _interopRequireDefault(_async);

var _config = require('./config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlFileSystem = exports.MlFileSystem = function (_MlConfig) {
	(0, _inherits3.default)(MlFileSystem, _MlConfig);

	function MlFileSystem() {
		(0, _classCallCheck3.default)(this, MlFileSystem);
		return (0, _possibleConstructorReturn3.default)(this, (MlFileSystem.__proto__ || Object.getPrototypeOf(MlFileSystem)).apply(this, arguments));
	}

	(0, _createClass3.default)(MlFileSystem, [{
		key: 'config',
		value: function config() {
			return {
				root: 'NoCloud/mlapp'
			};
		}
	}, {
		key: 'request',
		value: function request(localFileSystem) {
			var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
			var quotaSize = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

			var me = this;

			if (quotaSize === null) {
				if (size > 0) {
					quotaSize = size;
				} else {
					quotaSize = 61644800;
				}
			}

			var doRequest = function () {
				var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
					var fs, dir;
					return _regenerator2.default.wrap(function _callee$(_context) {
						while (1) {
							switch (_context.prev = _context.next) {
								case 0:
									_context.next = 2;
									return me.requestFileSystem(localFileSystem, size);

								case 2:
									fs = _context.sent;
									_context.next = 5;
									return me.mkdirp(fs.root, me.root);

								case 5:
									dir = _context.sent;
									_context.next = 8;
									return me.getFile(dir, '.nomedia', { create: true });

								case 8:
									return _context.abrupt('return', dir);

								case 9:
								case 'end':
									return _context.stop();
							}
						}
					}, _callee, this);
				}));

				return function doRequest() {
					return _ref.apply(this, arguments);
				};
			}();

			if (typeof navigator !== 'undefined' && navigator.webkitPersistentStorage) {
				return new Promise(function (resolve, reject) {
					var storage = localFileSystem === global.PERSISTENT ? navigator.webkitPersistentStorage : navigator.webkitTemporaryStorage;

					storage.requestQuota(quotaSize, function (grantedBytes) {
						me.grantedBytes = grantedBytes;
						resolve();
					}, reject);
				}).then(function () {
					return doRequest();
				});
			} else {
				return doRequest();
			}
		}
	}, {
		key: 'ls',
		value: function ls(dir) {
			return new Promise(function (resolve, reject) {
				dir.createReader().readEntries(resolve, reject);
			});
		}
	}, {
		key: 'mkdirp',
		value: function () {
			var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(dir, path) {
				var info, currentDir, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, subDir;

				return _regenerator2.default.wrap(function _callee2$(_context2) {
					while (1) {
						switch (_context2.prev = _context2.next) {
							case 0:
								info = MlFileSystem.info(path);
								currentDir = dir;
								_iteratorNormalCompletion = true;
								_didIteratorError = false;
								_iteratorError = undefined;
								_context2.prev = 5;
								_iterator = info.dirs[Symbol.iterator]();

							case 7:
								if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
									_context2.next = 15;
									break;
								}

								subDir = _step.value;
								_context2.next = 11;
								return this.getDirectory(currentDir, subDir, { create: true });

							case 11:
								currentDir = _context2.sent;

							case 12:
								_iteratorNormalCompletion = true;
								_context2.next = 7;
								break;

							case 15:
								_context2.next = 21;
								break;

							case 17:
								_context2.prev = 17;
								_context2.t0 = _context2['catch'](5);
								_didIteratorError = true;
								_iteratorError = _context2.t0;

							case 21:
								_context2.prev = 21;
								_context2.prev = 22;

								if (!_iteratorNormalCompletion && _iterator.return) {
									_iterator.return();
								}

							case 24:
								_context2.prev = 24;

								if (!_didIteratorError) {
									_context2.next = 27;
									break;
								}

								throw _iteratorError;

							case 27:
								return _context2.finish(24);

							case 28:
								return _context2.finish(21);

							case 29:
								return _context2.abrupt('return', currentDir);

							case 30:
							case 'end':
								return _context2.stop();
						}
					}
				}, _callee2, this, [[5, 17, 21, 29], [22,, 24, 28]]);
			}));

			function mkdirp(_x3, _x4) {
				return _ref2.apply(this, arguments);
			}

			return mkdirp;
		}()
	}, {
		key: 'saveBlob',
		value: function saveBlob(dir, blob, path) {
			return new Promise(function (resolve, reject) {
				_async2.default.waterfall([function (callback) {
					dir.getFile(path, { create: true }, function (entry) {
						callback(null, entry);
					}, callback);
				}, function (entry, callback) {
					entry.createWriter(function (writer) {
						writer.onwriteend = function () {
							callback(null, entry);
						};
						writer.onerror = callback;

						writer.write(blob);
					}, callback);
				}], function (err, entry) {
					if (err) {
						reject(err);
					} else {
						resolve(entry);
					}
				});
			});
		}
	}, {
		key: 'findOrCreate',
		value: function () {
			var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(dir, filePath) {
				return _regenerator2.default.wrap(function _callee3$(_context3) {
					while (1) {
						switch (_context3.prev = _context3.next) {
							case 0:
								_context3.prev = 0;
								_context3.next = 3;
								return this.getFile(dir, filePath, {});

							case 3:
								return _context3.abrupt('return', _context3.sent);

							case 6:
								_context3.prev = 6;
								_context3.t0 = _context3['catch'](0);

								if (!(_context3.t0.code === 1)) {
									_context3.next = 14;
									break;
								}

								_context3.next = 11;
								return this.getFile(dir, filePath, { create: true });

							case 11:
								return _context3.abrupt('return', _context3.sent);

							case 14:
								throw _context3.t0;

							case 15:
							case 'end':
								return _context3.stop();
						}
					}
				}, _callee3, this, [[0, 6]]);
			}));

			function findOrCreate(_x5, _x6) {
				return _ref3.apply(this, arguments);
			}

			return findOrCreate;
		}()
	}, {
		key: 'requestFileSystem',


		// FS Promise Wrappers
		value: function requestFileSystem(type, size) {
			return new Promise(function (resolve, reject) {
				(global.requestFileSystem || global.webkitRequestFileSystem)(type, size, resolve, reject);
			});
		}
	}, {
		key: 'getDirectory',
		value: function getDirectory(currentDir, subDir, options) {
			return new Promise(function (resolve, reject) {
				currentDir.getDirectory(subDir, options, function (newDir) {
					resolve(newDir);
				}, function (err) {
					reject(err);
				});
			});
		}
	}, {
		key: 'getFile',
		value: function getFile(currentDir, filePath, options) {
			return new Promise(function (resolve, reject) {
				currentDir.getFile(filePath, options, function (fileEntry) {
					resolve(fileEntry);
				}, function (err) {
					reject(err);
				});
			});
		}
	}], [{
		key: 'info',
		value: function info(path) {
			var dirs = path.split('/'),
			    lastIdx = dirs.length - 1,
			    fileName = dirs[lastIdx];

			var dotIdx = fileName.indexOf('.'),
			    name = fileName.substring(0, dotIdx),
			    ext = fileName.substring(dotIdx);

			return {
				dirs: dotIdx === -1 ? dirs : dirs.slice(0, lastIdx),
				name: name,
				ext: ext,
				fileName: fileName
			};
		}
	}, {
		key: 'test',
		value: function test() {
			new MlFileSystem().request(global.PERSISTENT).then(function (dir) {
				new MlFileSystem().supportsCdv(dir).then(function () {
					console.log(arguments);
				});return dir;
			}).then(function (dir) {
				return new MlFileSystem().mkdirp(dir, 'assets');
			}).then(function (dir) {
				console.log(dir.fullPath);return new MlFileSystem().ls(dir);
			}).then(function (entries) {
				console.log(entries);return entries;
			}).catch(function () {
				console.log(arguments);
			});
		}
	}]);
	return MlFileSystem;
}(_config.MlConfig);