'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Class for enabling infinite scroll on an element.
 */
var MlInfiniteScroll =

/**
 * Initialise with an options object.
 * @param  {Object} opts container: containing element,
 *                       content: inner element,
 *                       route: route to be triggered
 * @return {undefined}      Nothing is returned
 */
function MlInfiniteScroll(opts) {
	(0, _classCallCheck3.default)(this, MlInfiniteScroll);

	var me = this;

	opts = Object.assign({
		container: '',
		content: '',
		route: '',
		ajaxContainer: null,
		app: global.app
	}, opts);

	if (!opts.ajaxContainer) {
		opts.ajaxContainer = opts.content;
	}

	me.loadingMore = false;
	me.scrollTop = -1;

	function loadMore() {
		if (me.loadingMore || $(opts.container).height() <= $(opts.content).height()) {
			return Promise.resolve([]);
		}

		me.loadingMore = true;

		return opts.app.goTo(opts.route, null, false).then(function (collection) {
			me.loadingMore = false;

			if (collection && collection.length > 0) {
				return loadMore();
			} else {
				return Promise.resolve([]);
			}
		});
	};

	$(opts.container).on('scroll', function (evt) {
		if (me.loadingMore) {
			return;
		}

		me.loadingMore = true;

		var $self = $(this);

		var scrollTop = $self.scrollTop();

		/**
   * Prevents Ajax on reverse scroll
   */
		if (scrollTop <= me.scrollTop) {
			me.loadingMore = false;
			me.scrollTop = scrollTop;

			return;
		}

		me.scrollTop = scrollTop;

		var $wrapper = $self.children(opts.content);

		var containerHeight = $self.height(),
		    wrapperHeight = $wrapper.height();

		var pixelsFromBottom = wrapperHeight - (containerHeight + scrollTop);

		/**
   * Only trigger Ajax after threshold is hit
   */
		if (pixelsFromBottom < containerHeight / 2) {
			opts.app.goTo(opts.route, null, false).then(function () {
				me.loadingMore = false;
			});
		} else {
			me.loadingMore = false;
		}
	}).on('initialised', opts.ajaxContainer, function () {
		loadMore();
	});
};

exports.default = MlInfiniteScroll;