'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlApplication = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _config = require('./config');

var _progress = require('./progress');

var _pub_sub = require('./pub_sub');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlApplication = exports.MlApplication = function (_MlConfig) {
	(0, _inherits3.default)(MlApplication, _MlConfig);
	(0, _createClass3.default)(MlApplication, [{
		key: 'config',


		// Config

		value: function config() {
			return {
				controllers: [],
				classes: {
					router: require('./router').MlRouter,
					renderer: require('./renderer').MlRenderer
				}
			};
		}
	}]);

	function MlApplication() {
		(0, _classCallCheck3.default)(this, MlApplication);

		var _this = (0, _possibleConstructorReturn3.default)(this, (MlApplication.__proto__ || Object.getPrototypeOf(MlApplication)).call(this));

		_this.progress = new _progress.MlProgress();
		_this.event = new _pub_sub.MlPubSub();

		_this.router = new _this.classes.router({ app: _this, controllers: _this.controllers });

		if (_this.classes.renderer) {
			_this.renderer = new _this.classes.renderer();
		}

		_this.state = {};
		return _this;
	}

	// Overrides

	(0, _createClass3.default)(MlApplication, [{
		key: 'ready',
		value: function () {
			var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(onDevice) {
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								this.onDevice = onDevice;

							case 1:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, this);
			}));

			function ready(_x) {
				return _ref.apply(this, arguments);
			}

			return ready;
		}()
	}, {
		key: 'afterReady',
		value: function afterReady() {
			// Disable splash screen
			// $FlowFixMe
			if (typeof navigator !== 'undefined' && navigator.splashscreen && typeof navigator.splashscreen.hide === 'function') {
				// $FlowFixMe
				navigator.splashscreen.hide();
			}
		}

		// Helpers

	}], [{
		key: 'doAppReady',
		value: function doAppReady() {
			global.app = new this();

			function appReady(onDevice) {
				return function () {
					global.app.ready(onDevice).then(function () {
						return global.app.afterReady();
					});
				};
			}

			if (typeof global.cordova !== 'undefined') {
				document.addEventListener('deviceready', appReady(true), false);
			} else {
				$(appReady(false));
			}
		}
	}]);
	return MlApplication;
}(_config.MlConfig);