"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlConvert = undefined;

var _classCallCheck2 = require("babel-runtime/helpers/classCallCheck");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require("babel-runtime/helpers/createClass");

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlConvert = exports.MlConvert = function () {
	function MlConvert() {
		(0, _classCallCheck3.default)(this, MlConvert);
	}

	(0, _createClass3.default)(MlConvert, null, [{
		key: "objectToArray",
		value: function objectToArray(obj) {
			var arr = new Array();

			for (var key in obj) {
				arr.push({ key: key, val: obj[key] });
			}

			return arr;
		}
	}]);
	return MlConvert;
}();