'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlStateService = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _get2 = require('babel-runtime/helpers/get');

var _get3 = _interopRequireDefault(_get2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _service = require('./service');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlStateService = exports.MlStateService = function (_MlService) {
	(0, _inherits3.default)(MlStateService, _MlService);

	function MlStateService() {
		(0, _classCallCheck3.default)(this, MlStateService);
		return (0, _possibleConstructorReturn3.default)(this, (MlStateService.__proto__ || Object.getPrototypeOf(MlStateService)).apply(this, arguments));
	}

	(0, _createClass3.default)(MlStateService, [{
		key: 'config',
		value: function config() {
			var c = (0, _get3.default)(MlStateService.prototype.__proto__ || Object.getPrototypeOf(MlStateService.prototype), 'config', this).call(this);

			c.state = null;
			c.idField = null;

			return c;
		}
	}, {
		key: 'addToState',
		value: function addToState(resource) {
			this.state[resource[this.idField]] = resource;
		}
	}, {
		key: 'addAllToState',
		value: function addAllToState(collection) {
			var self = this;

			collection.forEach(function (resource) {
				self.addToState(resource);
			});

			return Promise.resolve(collection);
		}
	}, {
		key: 'getFromState',
		value: function getFromState(id) {
			var self = this;
			return Promise.resolve(self.state[id]);
		}
	}, {
		key: 'removeFromState',
		value: function removeFromState(id) {
			if (this.state[id]) {
				delete this.state[id];
			}
		}
	}]);
	return MlStateService;
}(_service.MlService);