'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlDownloader = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _config = require('./config');

var _file_system = require('./file_system');

var _proxy = require('./proxy');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlDownloader = exports.MlDownloader = function (_MlConfig) {
	(0, _inherits3.default)(MlDownloader, _MlConfig);

	function MlDownloader() {
		(0, _classCallCheck3.default)(this, MlDownloader);
		return (0, _possibleConstructorReturn3.default)(this, (MlDownloader.__proto__ || Object.getPrototypeOf(MlDownloader)).apply(this, arguments));
	}

	(0, _createClass3.default)(MlDownloader, [{
		key: 'config',
		value: function config() {
			return {
				namespace: 'namespace',
				rootPath: 'cdvfile://localhost/persistent',
				fileSystem: {
					class: _file_system.MlFileSystem,
					local: window.PERSISTENT
				},
				proxy: {
					class: _proxy.MlProxy
				}
			};
		}
	}, {
		key: 'getPath',
		value: function getPath(remoteUrl) {
			return this.stripHost(remoteUrl);
		}
	}, {
		key: 'stripHost',
		value: function stripHost(path) {
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = this.hosts[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var host = _step.value;

					path = path.replace(host, '');
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			return path;
		}
	}, {
		key: 'download',
		value: function download(remoteUrl, path, includeAuth) {
			if (typeof FileTransfer !== 'undefined') {
				return this.downloadFT(remoteUrl, path, includeAuth);
			} else {
				return this.downloadAjax(remoteUrl, path, includeAuth);
			}
		}
	}, {
		key: 'downloadOnly',
		value: function downloadOnly(remoteUrl, includeAuth) {
			var _this2 = this;

			return new Promise(function (resolve, reject) {
				var request = new XMLHttpRequest();

				request.open('GET', remoteUrl, true);
				request.responseType = 'blob';

				request.onerror = reject;
				request.onload = function () {
					resolve(request.response);
				};

				var headers = new _this2.proxy.class().allHeaders(includeAuth);
				for (var header in headers) {
					request.setRequestHeader(header, headers[header]);
				}

				request.send();
			});
		}
	}, {
		key: 'downloadAjax',
		value: function () {
			var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(remoteUrl, path, includeAuth) {
				var fs, blob, dir;
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								fs = new this.fileSystem.class();


								path = path || this.getPath(remoteUrl);

								_context.next = 4;
								return this.downloadOnly(remoteUrl, path, includeAuth);

							case 4:
								blob = _context.sent;
								_context.next = 7;
								return fs.request(this.fileSystem.local);

							case 7:
								dir = _context.sent;
								_context.next = 10;
								return fs.mkdirp(dir, path);

							case 10:
								dir = _context.sent;
								_context.next = 13;
								return fs.saveBlob(dir, blob, this.fileSystem.class.info(path).fileName);

							case 13:
								return _context.abrupt('return', _context.sent);

							case 14:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, this);
			}));

			function downloadAjax(_x, _x2, _x3) {
				return _ref.apply(this, arguments);
			}

			return downloadAjax;
		}()
	}, {
		key: 'downloadFT',
		value: function downloadFT(remoteUrl, path, includeAuth) {
			var me = this;

			var fs = new me.fileSystem.class();

			path = path || me.getPath(remoteUrl);

			return fs.request(me.fileSystem.local).then(function (dir) {
				return fs.mkdirp(dir, path);
			}).then(function (dir) {
				var fileTransfer = new FileTransfer(),
				    opts = { headers: new me.proxy.class().allHeaders(includeAuth) },
				    newPath = '' + me.rootPath + dir.fullPath + me.fileSystem.class.info(path).fileName;

				return new Promise(function (resolve, reject) {
					new FileTransfer().download(remoteUrl, newPath, resolve, reject, true, opts);
				});
			});
		}
	}], [{
		key: 'canDownload',
		value: function canDownload() {
			return typeof FileTransfer !== 'undefined' || typeof requestFileSystem !== 'undefined' || typeof webkitRequestFileSystem !== 'undefined';
		}
	}]);
	return MlDownloader;
}(_config.MlConfig);