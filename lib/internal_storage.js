'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlInternalStorage = undefined;

var _typeof2 = require('babel-runtime/helpers/typeof');

var _typeof3 = _interopRequireDefault(_typeof2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlInternalStorage = exports.MlInternalStorage = function () {
	(0, _createClass3.default)(MlInternalStorage, [{
		key: 'config',


		// Config
		value: function config(key) {
			this._key = key || 'store';

			this._storage = localforage;
		}
	}, {
		key: 'setSortField',
		value: function setSortField(field) {
			this._sortField = field;
		}

		// Contstructor

	}]);

	function MlInternalStorage(key) {
		(0, _classCallCheck3.default)(this, MlInternalStorage);

		this.config(key);

		this._sortField = null;
	}

	// Private


	(0, _createClass3.default)(MlInternalStorage, [{
		key: '_serialize',
		value: function _serialize(data) {
			return data;
		}
	}, {
		key: '_deserialize',
		value: function _deserialize(value) {
			return value;
		}
	}, {
		key: '_getRecordKey',
		value: function _getRecordKey(id) {
			return this._key + '-' + id;
		}
	}, {
		key: '_getIndexKey',
		value: function _getIndexKey() {
			return this._key + '_index';
		}
	}, {
		key: '_getIndex',
		value: function () {
			var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
				var item;
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								_context.next = 2;
								return this._storage.getItem(this._getIndexKey());

							case 2:
								item = _context.sent;
								return _context.abrupt('return', item ? this._deserialize(item) : []);

							case 4:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, this);
			}));

			function _getIndex() {
				return _ref.apply(this, arguments);
			}

			return _getIndex;
		}()
	}, {
		key: '_setIndex',
		value: function () {
			var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(array) {
				var obj;
				return _regenerator2.default.wrap(function _callee2$(_context2) {
					while (1) {
						switch (_context2.prev = _context2.next) {
							case 0:
								if (array) {
									_context2.next = 2;
									break;
								}

								return _context2.abrupt('return');

							case 2:
								obj = this._serialize(array);
								_context2.next = 5;
								return this._storage.setItem(this._getIndexKey(), obj);

							case 5:
								return _context2.abrupt('return', _context2.sent);

							case 6:
							case 'end':
								return _context2.stop();
						}
					}
				}, _callee2, this);
			}));

			function _setIndex(_x) {
				return _ref2.apply(this, arguments);
			}

			return _setIndex;
		}()
	}, {
		key: '_addToIndex',
		value: function () {
			var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(recordKey) {
				var index;
				return _regenerator2.default.wrap(function _callee3$(_context3) {
					while (1) {
						switch (_context3.prev = _context3.next) {
							case 0:
								_context3.next = 2;
								return this._getIndex();

							case 2:
								index = _context3.sent;

								if (!(!index || index.includes(recordKey))) {
									_context3.next = 5;
									break;
								}

								return _context3.abrupt('return');

							case 5:

								index.push(recordKey);
								_context3.next = 8;
								return this._setIndex(index);

							case 8:
							case 'end':
								return _context3.stop();
						}
					}
				}, _callee3, this);
			}));

			function _addToIndex(_x2) {
				return _ref3.apply(this, arguments);
			}

			return _addToIndex;
		}()
	}, {
		key: '_removeFromIndex',
		value: function () {
			var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(recordKey) {
				var index, idx;
				return _regenerator2.default.wrap(function _callee4$(_context4) {
					while (1) {
						switch (_context4.prev = _context4.next) {
							case 0:
								_context4.next = 2;
								return this._getIndex();

							case 2:
								index = _context4.sent;
								idx = index ? index.indexOf(recordKey) : -1;


								if (idx !== -1) {
									index.splice(idx, 1);
								}

								_context4.next = 7;
								return this._setIndex(index);

							case 7:
							case 'end':
								return _context4.stop();
						}
					}
				}, _callee4, this);
			}));

			function _removeFromIndex(_x3) {
				return _ref4.apply(this, arguments);
			}

			return _removeFromIndex;
		}()
	}, {
		key: '_hasKey',
		value: function () {
			var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(id) {
				var index;
				return _regenerator2.default.wrap(function _callee5$(_context5) {
					while (1) {
						switch (_context5.prev = _context5.next) {
							case 0:
								_context5.next = 2;
								return this._getIndex();

							case 2:
								index = _context5.sent;
								return _context5.abrupt('return', index.includes(this._getRecordKey(id)));

							case 4:
							case 'end':
								return _context5.stop();
						}
					}
				}, _callee5, this);
			}));

			function _hasKey(_x4) {
				return _ref5.apply(this, arguments);
			}

			return _hasKey;
		}()
	}, {
		key: '_getRecord',
		value: function () {
			var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(recordKey) {
				var record;
				return _regenerator2.default.wrap(function _callee6$(_context6) {
					while (1) {
						switch (_context6.prev = _context6.next) {
							case 0:
								_context6.next = 2;
								return this._storage.getItem(recordKey);

							case 2:
								record = _context6.sent;
								return _context6.abrupt('return', this._deserialize(record));

							case 4:
							case 'end':
								return _context6.stop();
						}
					}
				}, _callee6, this);
			}));

			function _getRecord(_x5) {
				return _ref6.apply(this, arguments);
			}

			return _getRecord;
		}()
	}, {
		key: '_setRecord',
		value: function () {
			var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7(recordKey, data) {
				return _regenerator2.default.wrap(function _callee7$(_context7) {
					while (1) {
						switch (_context7.prev = _context7.next) {
							case 0:
								_context7.next = 2;
								return this._storage.setItem(recordKey, this._serialize(data));

							case 2:
								_context7.next = 4;
								return this._addToIndex(recordKey);

							case 4:
							case 'end':
								return _context7.stop();
						}
					}
				}, _callee7, this);
			}));

			function _setRecord(_x6, _x7) {
				return _ref7.apply(this, arguments);
			}

			return _setRecord;
		}()
	}, {
		key: '_removeRecord',
		value: function () {
			var _ref8 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8(recordKey) {
				return _regenerator2.default.wrap(function _callee8$(_context8) {
					while (1) {
						switch (_context8.prev = _context8.next) {
							case 0:
								_context8.next = 2;
								return this._storage.removeItem(recordKey);

							case 2:
								_context8.next = 4;
								return this._removeFromIndex(recordKey);

							case 4:
							case 'end':
								return _context8.stop();
						}
					}
				}, _callee8, this);
			}));

			function _removeRecord(_x8) {
				return _ref8.apply(this, arguments);
			}

			return _removeRecord;
		}()
	}, {
		key: '_sort',
		value: function () {
			var _ref9 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9() {
				var _this = this;

				var array, newIndex;
				return _regenerator2.default.wrap(function _callee9$(_context9) {
					while (1) {
						switch (_context9.prev = _context9.next) {
							case 0:
								if (this._sortField) {
									_context9.next = 2;
									break;
								}

								return _context9.abrupt('return');

							case 2:
								_context9.next = 4;
								return this._getAll();

							case 4:
								array = _context9.sent;

								array.sort(function (a, b) {
									return +(a[_this._sortField] > b[_this._sortField]);
								});

								newIndex = array.map(function (x) {
									return _this._getRecordKey(x.id);
								});
								_context9.next = 9;
								return this._setIndex(newIndex);

							case 9:
							case 'end':
								return _context9.stop();
						}
					}
				}, _callee9, this);
			}));

			function _sort() {
				return _ref9.apply(this, arguments);
			}

			return _sort;
		}()
	}, {
		key: '_getAll',
		value: function () {
			var _ref10 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee10() {
				var index, rv, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, recordKey;

				return _regenerator2.default.wrap(function _callee10$(_context10) {
					while (1) {
						switch (_context10.prev = _context10.next) {
							case 0:
								_context10.next = 2;
								return this._getIndex();

							case 2:
								index = _context10.sent;
								rv = [];
								_iteratorNormalCompletion = true;
								_didIteratorError = false;
								_iteratorError = undefined;
								_context10.prev = 7;
								_iterator = index[Symbol.iterator]();

							case 9:
								if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
									_context10.next = 19;
									break;
								}

								recordKey = _step.value;
								_context10.t0 = rv;
								_context10.next = 14;
								return this._getRecord(recordKey);

							case 14:
								_context10.t1 = _context10.sent;

								_context10.t0.push.call(_context10.t0, _context10.t1);

							case 16:
								_iteratorNormalCompletion = true;
								_context10.next = 9;
								break;

							case 19:
								_context10.next = 25;
								break;

							case 21:
								_context10.prev = 21;
								_context10.t2 = _context10['catch'](7);
								_didIteratorError = true;
								_iteratorError = _context10.t2;

							case 25:
								_context10.prev = 25;
								_context10.prev = 26;

								if (!_iteratorNormalCompletion && _iterator.return) {
									_iterator.return();
								}

							case 28:
								_context10.prev = 28;

								if (!_didIteratorError) {
									_context10.next = 31;
									break;
								}

								throw _iteratorError;

							case 31:
								return _context10.finish(28);

							case 32:
								return _context10.finish(25);

							case 33:
								return _context10.abrupt('return', rv);

							case 34:
							case 'end':
								return _context10.stop();
						}
					}
				}, _callee10, this, [[7, 21, 25, 33], [26,, 28, 32]]);
			}));

			function _getAll() {
				return _ref10.apply(this, arguments);
			}

			return _getAll;
		}()
	}, {
		key: '_setAll',
		value: function () {
			var _ref11 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee11(array) {
				var clearFirst = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

				var _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, obj, recordKey;

				return _regenerator2.default.wrap(function _callee11$(_context11) {
					while (1) {
						switch (_context11.prev = _context11.next) {
							case 0:
								if (!clearFirst) {
									_context11.next = 3;
									break;
								}

								_context11.next = 3;
								return this._clear();

							case 3:
								_iteratorNormalCompletion2 = true;
								_didIteratorError2 = false;
								_iteratorError2 = undefined;
								_context11.prev = 6;
								_iterator2 = array[Symbol.iterator]();

							case 8:
								if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
									_context11.next = 18;
									break;
								}

								obj = _step2.value;
								recordKey = this._getRecordKey(obj.id);
								_context11.next = 13;
								return this._setRecord(recordKey, obj);

							case 13:
								_context11.next = 15;
								return this._addToIndex(recordKey);

							case 15:
								_iteratorNormalCompletion2 = true;
								_context11.next = 8;
								break;

							case 18:
								_context11.next = 24;
								break;

							case 20:
								_context11.prev = 20;
								_context11.t0 = _context11['catch'](6);
								_didIteratorError2 = true;
								_iteratorError2 = _context11.t0;

							case 24:
								_context11.prev = 24;
								_context11.prev = 25;

								if (!_iteratorNormalCompletion2 && _iterator2.return) {
									_iterator2.return();
								}

							case 27:
								_context11.prev = 27;

								if (!_didIteratorError2) {
									_context11.next = 30;
									break;
								}

								throw _iteratorError2;

							case 30:
								return _context11.finish(27);

							case 31:
								return _context11.finish(24);

							case 32:
							case 'end':
								return _context11.stop();
						}
					}
				}, _callee11, this, [[6, 20, 24, 32], [25,, 27, 31]]);
			}));

			function _setAll(_x9) {
				return _ref11.apply(this, arguments);
			}

			return _setAll;
		}()
	}, {
		key: '_clear',
		value: function () {
			var _ref12 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee12() {
				var index, _iteratorNormalCompletion3, _didIteratorError3, _iteratorError3, _iterator3, _step3, recordKey;

				return _regenerator2.default.wrap(function _callee12$(_context12) {
					while (1) {
						switch (_context12.prev = _context12.next) {
							case 0:
								_context12.next = 2;
								return this._getIndex();

							case 2:
								index = _context12.sent;
								_iteratorNormalCompletion3 = true;
								_didIteratorError3 = false;
								_iteratorError3 = undefined;
								_context12.prev = 6;
								_iterator3 = index[Symbol.iterator]();

							case 8:
								if (_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done) {
									_context12.next = 15;
									break;
								}

								recordKey = _step3.value;
								_context12.next = 12;
								return this._storage.removeItem(recordKey);

							case 12:
								_iteratorNormalCompletion3 = true;
								_context12.next = 8;
								break;

							case 15:
								_context12.next = 21;
								break;

							case 17:
								_context12.prev = 17;
								_context12.t0 = _context12['catch'](6);
								_didIteratorError3 = true;
								_iteratorError3 = _context12.t0;

							case 21:
								_context12.prev = 21;
								_context12.prev = 22;

								if (!_iteratorNormalCompletion3 && _iterator3.return) {
									_iterator3.return();
								}

							case 24:
								_context12.prev = 24;

								if (!_didIteratorError3) {
									_context12.next = 27;
									break;
								}

								throw _iteratorError3;

							case 27:
								return _context12.finish(24);

							case 28:
								return _context12.finish(21);

							case 29:
								_context12.next = 31;
								return this._storage.removeItem(this._getIndexKey());

							case 31:
							case 'end':
								return _context12.stop();
						}
					}
				}, _callee12, this, [[6, 17, 21, 29], [22,, 24, 28]]);
			}));

			function _clear() {
				return _ref12.apply(this, arguments);
			}

			return _clear;
		}()

		// Public

	}, {
		key: 'has',
		value: function has(id) {
			return this._hasKey(id);
		}
	}, {
		key: 'get',
		value: function get(id) {
			if (id === undefined || id === null) {
				return this._getAll();
			} else {
				return this._getRecord(this._getRecordKey(id));
			}
		}
	}, {
		key: 'first',
		value: function () {
			var _ref13 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee13() {
				var collection;
				return _regenerator2.default.wrap(function _callee13$(_context13) {
					while (1) {
						switch (_context13.prev = _context13.next) {
							case 0:
								_context13.next = 2;
								return this._getAll();

							case 2:
								collection = _context13.sent;
								return _context13.abrupt('return', collection && collection.length > 0 ? collection[0] : null);

							case 4:
							case 'end':
								return _context13.stop();
						}
					}
				}, _callee13, this);
			}));

			function first() {
				return _ref13.apply(this, arguments);
			}

			return first;
		}()
	}, {
		key: 'set',
		value: function () {
			var _ref14 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee14(id) {
				var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

				var _iteratorNormalCompletion4, _didIteratorError4, _iteratorError4, _iterator4, _step4, key;

				return _regenerator2.default.wrap(function _callee14$(_context14) {
					while (1) {
						switch (_context14.prev = _context14.next) {
							case 0:
								if (!((typeof id === 'undefined' ? 'undefined' : (0, _typeof3.default)(id)) === 'object')) {
									_context14.next = 41;
									break;
								}

								if (!(Array.isArray(id) && typeof data === 'boolean')) {
									_context14.next = 7;
									break;
								}

								_context14.next = 4;
								return this._setAll(id, data);

							case 4:
								return _context14.abrupt('return', _context14.sent);

							case 7:
								if (!(id.id !== undefined)) {
									_context14.next = 13;
									break;
								}

								_context14.next = 10;
								return this._setRecord(this._getRecordKey(id.id), id);

							case 10:
								return _context14.abrupt('return', _context14.sent);

							case 13:
								_iteratorNormalCompletion4 = true;
								_didIteratorError4 = false;
								_iteratorError4 = undefined;
								_context14.prev = 16;
								_iterator4 = Object.keys(id)[Symbol.iterator]();

							case 18:
								if (_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done) {
									_context14.next = 25;
									break;
								}

								key = _step4.value;
								_context14.next = 22;
								return this._setRecord(this._getRecordKey(key), id[key]);

							case 22:
								_iteratorNormalCompletion4 = true;
								_context14.next = 18;
								break;

							case 25:
								_context14.next = 31;
								break;

							case 27:
								_context14.prev = 27;
								_context14.t0 = _context14['catch'](16);
								_didIteratorError4 = true;
								_iteratorError4 = _context14.t0;

							case 31:
								_context14.prev = 31;
								_context14.prev = 32;

								if (!_iteratorNormalCompletion4 && _iterator4.return) {
									_iterator4.return();
								}

							case 34:
								_context14.prev = 34;

								if (!_didIteratorError4) {
									_context14.next = 37;
									break;
								}

								throw _iteratorError4;

							case 37:
								return _context14.finish(34);

							case 38:
								return _context14.finish(31);

							case 39:
								_context14.next = 44;
								break;

							case 41:
								_context14.next = 43;
								return this._setRecord(this._getRecordKey(id), data);

							case 43:
								return _context14.abrupt('return', _context14.sent);

							case 44:
							case 'end':
								return _context14.stop();
						}
					}
				}, _callee14, this, [[16, 27, 31, 39], [32,, 34, 38]]);
			}));

			function set(_x11) {
				return _ref14.apply(this, arguments);
			}

			return set;
		}()
	}, {
		key: 'update',
		value: function () {
			var _ref15 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee15(id, data) {
				var recordKey, record;
				return _regenerator2.default.wrap(function _callee15$(_context15) {
					while (1) {
						switch (_context15.prev = _context15.next) {
							case 0:
								recordKey = this._getRecordKey(id);
								_context15.next = 3;
								return this._getRecord(recordKey);

							case 3:
								record = _context15.sent;

								if (!record) {
									_context15.next = 9;
									break;
								}

								Object.assign(record, data);
								_context15.next = 8;
								return this._setRecord(recordKey, record);

							case 8:
								;

							case 9:
							case 'end':
								return _context15.stop();
						}
					}
				}, _callee15, this);
			}));

			function update(_x13, _x14) {
				return _ref15.apply(this, arguments);
			}

			return update;
		}()
	}, {
		key: 'remove',
		value: function remove(id) {
			return this._removeRecord(this._getRecordKey(id));
		}
	}, {
		key: 'clear',
		value: function clear() {
			return this._clear();
		}
	}, {
		key: 'count',
		value: function () {
			var _ref16 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee16() {
				var index;
				return _regenerator2.default.wrap(function _callee16$(_context16) {
					while (1) {
						switch (_context16.prev = _context16.next) {
							case 0:
								_context16.next = 2;
								return this._getIndex();

							case 2:
								index = _context16.sent;
								return _context16.abrupt('return', index ? index.length : 0);

							case 4:
							case 'end':
								return _context16.stop();
						}
					}
				}, _callee16, this);
			}));

			function count() {
				return _ref16.apply(this, arguments);
			}

			return count;
		}()
	}, {
		key: 'sort',
		value: function sort() {
			return this._sort();
		}
	}]);
	return MlInternalStorage;
}();