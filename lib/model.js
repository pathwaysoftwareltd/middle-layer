'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlModel = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _config = require('./config');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlModel = exports.MlModel = function (_MlConfig) {
	(0, _inherits3.default)(MlModel, _MlConfig);

	function MlModel() {
		(0, _classCallCheck3.default)(this, MlModel);
		return (0, _possibleConstructorReturn3.default)(this, (MlModel.__proto__ || Object.getPrototypeOf(MlModel)).apply(this, arguments));
	}

	(0, _createClass3.default)(MlModel, [{
		key: 'convertResource',
		value: function convertResource(obj) {
			var self = this;

			var resource = Object.assign({}, obj);

			for (var fieldType in self.fieldTypes) {
				if (!resource[fieldType]) {
					continue;
				}

				var convertor = self.fieldTypes[fieldType];

				if (typeof convertor === 'string') {
					switch (convertor) {
						case 'date':
							var m = fieldType + 'Moment';

							resource[m] = moment(resource[fieldType]);
							resource[fieldType] = resource[m].format('lll');

							break;
					}
				} else {
					resource[fieldType] = convertor(resource[fieldType]);
				}
			}

			return resource;
		}
	}, {
		key: 'convertCollection',
		value: function convertCollection(arr) {
			var self = this;

			var collection = [];

			arr.forEach(function (obj) {
				collection.push(self.convertResource(obj));
			});

			return collection;
		}
	}, {
		key: 'convert',
		value: function convert(val) {
			if (Array.isArray(val)) {
				return this.convertCollection(val);
			} else {
				return this.convertResource(val);
			}
		}
	}]);
	return MlModel;
}(_config.MlConfig);