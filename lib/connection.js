'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlConnection = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _pub_sub = require('./pub_sub');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlConnection = exports.MlConnection = function () {
	function MlConnection() {
		(0, _classCallCheck3.default)(this, MlConnection);

		this.attached = false;
		this.pluginDetected = typeof navigator !== 'undefined' && navigator.connection && typeof Connection !== 'undefined';

		this.event = new _pub_sub.MlPubSub();

		this.boundHandler = this.handler.bind(this);
	}

	(0, _createClass3.default)(MlConnection, [{
		key: 'isOnline',
		value: function isOnline() {
			return !this.pluginDetected || navigator.connection.type !== Connection.NONE;
		}
	}, {
		key: 'attach',
		value: function attach() {
			if (this.attached) {
				return false;
			}

			document.addEventListener('online', this.boundHandler, false);
			document.addEventListener('offline', this.boundHandler, false);

			this.attached = true;
		}
	}, {
		key: 'detach',
		value: function detach() {
			if (!this.attached) {
				return false;
			}

			document.removeEventListener('online', this.boundHandler, false);
			document.removeEventListener('offline', this.boundHandler, false);

			this.attached = false;
		}
	}, {
		key: 'subscribe',
		value: function subscribe(func) {
			return this.event.subscribe(0, func);
		}
	}, {
		key: 'handler',
		value: function handler(evt) {
			this.event.publish(0, evt.type === 'online');
		}
	}]);
	return MlConnection;
}();