'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlFlash = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlFlash = function () {
	function MlFlash() {
		(0, _classCallCheck3.default)(this, MlFlash);
	}

	(0, _createClass3.default)(MlFlash, null, [{
		key: 'message',
		value: function () {
			var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_message, type) {
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								if (!(typeof navigator !== 'undefined' && navigator.notification)) {
									_context.next = 4;
									break;
								}

								return _context.abrupt('return', new Promise(function (resolve) {
									navigator.notification.alert(_message, // message
									function () {
										resolve();
									}, // callback
									'Forget Me Not', // title
									'Okay' // buttonName
									);
								}));

							case 4:
								alert(_message);

							case 5:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, this);
			}));

			function message(_x, _x2) {
				return _ref.apply(this, arguments);
			}

			return message;
		}()
	}, {
		key: 'alert',
		value: function alert(message) {
			return this.message(message, 'alert');
		}
	}, {
		key: 'info',
		value: function info(message) {
			return this.message(message, 'info');
		}
	}]);
	return MlFlash;
}();

exports.MlFlash = MlFlash;