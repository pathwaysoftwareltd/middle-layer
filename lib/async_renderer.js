'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MlAsyncRenderer = undefined;

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _async = require('async');

var _async2 = _interopRequireDefault(_async);

var _renderer = require('./renderer');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MlAsyncRenderer = exports.MlAsyncRenderer = function (_MlRenderer) {
	(0, _inherits3.default)(MlAsyncRenderer, _MlRenderer);

	function MlAsyncRenderer(env) {
		(0, _classCallCheck3.default)(this, MlAsyncRenderer);

		var _this = (0, _possibleConstructorReturn3.default)(this, (MlAsyncRenderer.__proto__ || Object.getPrototypeOf(MlAsyncRenderer)).call(this, env));

		var self = _this;

		self.instructions = [];
		return _this;
	}

	(0, _createClass3.default)(MlAsyncRenderer, [{
		key: 'instruct',
		value: function instruct() {
			var opts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

			var self = this;

			opts = Object.assign({
				template: null,
				selector: null,
				promise: null
			}, opts);

			self.instructions.push(opts);

			return self;
		}
	}, {
		key: 'renderInstructions',
		value: function renderInstructions($target) {
			var self = this;

			return new Promise(function (resolve, reject) {
				if (!self.instructions.length) {
					resolve();
					return false;
				}

				_async2.default.each(self.instructions, function (instruction, callback) {
					instruction.promise.then(function (data) {
						return self.render(instruction.template, data);
					}).then(function (html) {
						$target.find(instruction.selector).replaceWith(html);

						callback();
					});
				}, function (err) {
					resolve();
				});
			});
		}
	}]);
	return MlAsyncRenderer;
}(_renderer.MlRenderer);